<?php

/**
 *
 * @link              http://keychamps.com/
 * @since             1.0
 * @package           Ultimate Animations For WooCommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Ultimate Animations For WooCommerce
 * Plugin URI:        http://keychamps.com/
 * Description:       A Simple Plugin to change WooCommerce SKU Label to ISBN or any other custom text.
 * Version:           1.0
 * Author:            Asaduzzaman Abir
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       uanimation-woo
 *//**
 * Filter the cart template path to use our cart.php template instead of the theme's
 */

// https://wpdevdesign.com/how-to-override-woocommerce-templates-using-a-custom-functionality-plugin/

// Simple product add to cart Override Function

function csp_locate_template( $template, $template_name, $template_path ) {
	$basename = basename( $template );
	if( $basename == 'simple.php' ) {
		$template = trailingslashit( plugin_dir_path( __FILE__ ) ) . 'templates/simple.php';
	}
	return $template;
}
add_filter( 'woocommerce_locate_template', 'csp_locate_template', 10, 3 );


add_action('wp_enqueue_scripts','uanimation_woo_all_assets');

function uanimation_woo_all_assets(){
	wp_enqueue_style('uanimation_woo_animation',plugin_dir_url(__FILE__)."scripts/uanimation-woo.css");
	//wp_enqueue_script('uanimation_woo_lessjs',plugin_dir_url(__FILE__)."scripts/less/dist/less.min.js",1.0,True);
}


class WooCommerce_Ultimate_Animation_Settings_Page {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'uanimation_woo_create_settings' ) );
		add_action( 'admin_init', array( $this, 'uanimation_woo_setup_sections' ) );
		add_action( 'admin_init', array( $this, 'uanimation_woo_setup_fields' ) );
		//add_action( 'plugins_loaded', array( $this, 'optionsdemo_bootstrap' ) );
		//add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), array( $this, 'optionsdemo_settings_link' ) );
	}

	/* public function optionsdemo_settings_link($links) {
	    $newlink = sprintf("<a href='%s'>%s</a>",'options-general.php?page=optionsdemo',__('Settings','optionsdemo'));
	    $links[] = $newlink;
	    return $links;
	}*/


	/*public function optionsdemo_bootstrap() {
		load_plugin_textdomain( 'optionsdemo', false, plugin_dir_path( __FILE__ ) . "/languages" );
	}*/

	public function uanimation_woo_create_settings() {
		$page_title = __( 'Ultimate Animation For WooCommerce', 'uanimation-woo' );
		$menu_title = __( 'Ultimate Animation For WooCommerce', 'uanimation-woo' );
		$capability = 'manage_options';
		$slug       = 'uanimation-woo';
		$callback   = array( $this, 'uanimation_woo_settings_content' );
		add_menu_page( $page_title, $menu_title, $capability, $slug, $callback );
	}

	public function uanimation_woo_settings_content() { ?>
        <div class="wrap">
            <h1><?php _e('Ultimate Animation For WooCommerce','uanimation-woo' ); ?></h1>
            <form method="POST" action="options.php">
				<?php
				settings_fields( 'uanimation-woo' );
				do_settings_sections( 'uanimation-woo' );
				submit_button();
				?>
            </form>
        </div> <?php
	}

	public function uanimation_woo_setup_sections() {
		add_settings_section( 'uanimation_woo_section', __('Demonstration of plugin settings page', 'uanimation-woo' ), array(), 'uanimation-woo' );
	}

	public function uanimation_woo_setup_fields() {
		$fields = array(
			array(
				'label'       => __( 'Library', 'uanimation-woo' ),
				'id'          => 'uanimation_woo_library',
				'type'        => 'text',
				'section'     => 'uanimation_woo_section',
				'placeholder' => __( 'Library', 'uanimation-woo' ),
			),
			array(
				'label'       => __( 'Animation Duration', 'uanimation-woo' ),
				'id'          => 'uanimation_woo_animation_duration',
				'type'        => 'text',
				'section'     => 'uanimation_woo_section',
				'placeholder' => __( 'Animation Duration', 'uanimation-woo' ),
			),
			array(
				'label'       => __( 'Animation Name', 'uanimation-woo' ),
				'id'          => 'uanimation_woo_animation_name',
				'type'        => 'text',
				'section'     => 'uanimation_woo_section',
				'placeholder' => __( 'Animation Name', 'uanimation-woo' ),
			),
			array(
				'label'       => __( 'Animation Delay', 'uanimation-woo' ),
				'id'          => 'uanimation_woo_animation_delay',
				'type'        => 'text',
				'section'     => 'uanimation_woo_section',
				'placeholder' => __( 'Animation Delay', 'uanimation-woo' ),
			),
			
		);
		foreach ( $fields as $field ) {
			add_settings_field( $field['id'], $field['label'], array(
				$this,
				'uanimation_woo_field_callback'
			), 'uanimation-woo', $field['section'], $field );
			register_setting( 'uanimation-woo', $field['id'] );
		}
	}

	public function uanimation_woo_field_callback( $field ) {
		$value = get_option( $field['id'] );
		switch ( $field['type'] ) {
			case 'textarea':
				printf( '<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>',
					$field['id'],
					isset( $field['placeholder'] ) ? $field['placeholder'] : '',
					$value
				);
				break;
			default:
				printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />',
					$field['id'],
					$field['type'],
					isset( $field['placeholder'] ) ? $field['placeholder'] : '',
					$value
				);
		}
		if ( isset( $field['desc'] ) ) {
			if ( $desc = $field['desc'] ) {
				printf( '<p class="description">%s </p>', $desc );
			}
		}
	}
}

new WooCommerce_Ultimate_Animation_Settings_Page();?>



=== Ultimate Animations For WooCommerce ===
Contributors: asadabir
Tags: woocommerce,sku,sku to isbn,product sku
Donate link: https://keychamps.com
Requires at least: 3.0.1
Tested up to: 5.3.2
Requires PHP: 5.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Simple Plugin to change WooCommerce SKU Label to ISBN or any other custom text.

== Description ==
Ultimate Animations For WooCommerce is a simple but effective plugin for WooCommerce users. Most of the users wants to change the default SKU label. But, unfortunately they don\'t understand the codes. Also, hiring a web developer is not a good idea for change the simple text.

This plugin allows to to change the text easily. Here are the steps that you need to done.

- Install the plugin from WordPress.org Repository.
- Go to \'General\' under settings page
- You will find a textbox named \"WooCommerce SKU Label\"
- Enter your desired text and hit the save button.
- Done

== Frequently Asked Questions ==
Does the plugin require any special settings?
Ans: No

If I will disable the plugin, what will happen?
Ans: It will revert to default \'SKU\' from your custom text.

== Screenshots ==
1. Settings Page
2. WooCommerce Backend
3. WooCommerce Frontend

== Changelog ==

1.0: Initial Release